﻿using System;
using TechTalk.SpecFlow;
using IntegrationnTest.IngTrading.PageObjects;

namespace IntegrationTest.TestRunner.Bindings
{
    

    [Binding]
    public class OnlineTradingSteps
    {
        LoginPage login = new LoginPage();

        [Given(@"I launch Online Trading Web site")]
        public void GivenILaunchOnlineTradingWebSite()
        {
            login.LoadWebSite();
        }
        
        [When(@"I enter login detals")]
        public void WhenIEnterLoginDetals()
        {
            login.EnterLoginDetails();
        }
        
        [Then(@"user is loggged in successfully")]
        public void ThenUserIsLogggedInSuccessfully()
        {
            
        }

        [Then(@"user is loggged in successfully and it display stock selection page with Title '(.*)'")]
        public void ThenUserIsLogggedInSuccessfullyAndItDisplayStockSelectionPageWithTitle(string title)
        { 
            login.ValidateLoginFuntion(title);
        }

    }
}
