﻿Feature: OnlineTrading
	In order to make profit from online trading platform
	As a trading user
	I want to purchase and  hold stocks

@login
Scenario: Login to Online Trading Platform
	Given I launch Online Trading Web site
	When I enter login detals
	Then user is loggged in successfully and it display stock selection page with Title 'Select the Stock Name'

Scenario: Purchase stock by trading user
	Given I launch Online Trading Web site
	When login to Onine Trading Platform
	And select stock 'HCL' from stock selection page
	And enter quantity of stock
	And Purchase the stock
	Then Purchase should be successful and it dispplay confirmation message 'Ordered  successfully'

Scenario: Viewing the purchase history
	Given I launch Online Trading Web site
	When login to Onine Trading Platform
	And purchased the stock 'HCL'
	And view the purchase history
	Then It  displays above stock in purchase history page