﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntegrationnTest.IngTrading.Repository;
using System.Threading;
using OpenQA.Selenium.Firefox;
using SeleniumExtras.PageObjects;

namespace IntegrationnTest.IngTrading.PageObjects
{
    public class LoginPage
    {

        IWebDriver driver;
        LoginRepo repo = new LoginRepo();  
        

        ~LoginPage()
        {
            driver.Quit();
        }

        public void LoadWebSite()
        {
            driver = new FirefoxDriver();
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            PageFactory.InitElements(driver, repo);
            driver.Navigate().GoToUrl("http://google.com");
        }

        public void EnterLoginDetails()
        {
            
            repo.userName.SendKeys("username1");

            repo.loginBtn.Click();
        }

        public void ValidateLoginFuntion(string StockPageTitle)
        {
            string title = driver.Title;

            if(title.Trim()!=StockPageTitle)
            {
                throw new Exception("Stock page is not  launched after login");
            }

        }
}
}
